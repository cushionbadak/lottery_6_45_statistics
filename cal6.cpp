#include "cal6.h"
#include <algorithm>
#include <iostream>
#include <fstream>
#include <memory>
#include <sstream>

//#define DEBUGING

using namespace std;
using namespace cal6;

Error::Error (EC ec) : error_case{ec} {}; // Error::Error

void Error::FileReadErrorMessage (void){
  cout << "**Error when file reading." << endl << "  ";
  switch (error_case){
    case EC::NEED6 :
      cout << "It needs at least 6 numbers at one line.";
      break;
    case EC::NANUM :
      cout << "Some numbers has non-appropriate numbers or characters.";
      break;
  }
  cout << endl << "  Inappropriate value will fill an array." << endl;
} // Error::FileReadErrorMessage

array<int, 6> Raw::MakeSixIntegers(const string &s){
  stringstream ss {s};
  string item {""};
  int index {0};
  int t {0};
  array<int, 6> arr {0,0,0,0,0,0};
  while(std::getline(ss, item, ' ')) {
    try {
      t = atoi(item.c_str());
    } catch (std::exception) { throw Error(EC::NANUM); }
    if( t < 1 || t > 45 ) { throw Error(EC::NANUM); }
    else {
      arr[index++] = atoi(item.c_str());
      if(index > 5) {break;}
    }
  }
  if(index < 6) {throw Error(EC::NEED6);}
  return arr;
} // Raw::MakeSixIntegers

Raw::Raw(const char* filename) : size{0}, datas{} {
  fstream ifs {filename, ifstream::in};
  unique_ptr<char[]> cptr {new char[128]};
  string tmpstr {""};
  array<int, 6> arr {1,1,1,1,1,1};
  while(ifs.getline(cptr.get(), 128)) {
    tmpstr = cptr.get();
    try{
      arr = (MakeSixIntegers(tmpstr));
    } catch (Error e) { e.FileReadErrorMessage(); }
    datas.push_back(arr);
    arr = {1,1,1,1,1,1};
  }
}; // Raw::Raw

void Raw::PrintDatas(void) {
  for (auto i : datas){
    for (auto j : i){
      cout << j << ' ';
    }
    cout << endl;
  }
} // Raw::PrintDatas

const vector< array<int, 6> >& Raw::GetDatas(void){
  return datas;
} // Raw::GetDatas

NumTable1::NumTable1 (Raw r) : init_datas{}, datas{}, init_count{0}, count{0} {
  array<int, 2> tmparr {1,1};
  for(auto arr : r.GetDatas()){
    auto ir = init_datas.find(arr[0]);
    if(ir != init_datas.end()){
      ++init_datas[arr[0]];
    } else {
      init_datas.insert(std::pair<int, int>{arr[0], 1});
    }
    ++init_count;
    for(unsigned int i=0; i<arr.size()-1; i++){
      tmparr[0] = arr[i];
      tmparr[1] = arr[i+1];
      auto r = datas.find(tmparr);
      if(r != datas.end()){
        ++datas[tmparr];
      } else {
        // NOT FOUND
        datas.insert(std::pair<array<int, 2>, int>{tmparr, 1});
      }
      ++count;
    }
  }
}; // NumTable1::NumTable1

void NumTable1::PrintDatas (void){
  for(auto i : datas){
    cout << i.first[0] << '\t' << i.first[1] << "\t:\t" << i.second << endl;
  }
} // NumTable1::PrintDatas

double NumTable1::AppearFrequencyNoInit (array<int, 2> arr){
  if(datas.find(arr) != datas.end()){
    return (double)datas[arr] / count;
  } else {
    return 0.0;
  }
} // NumTable1::AppearFrequencyNoInit (array<int, 2>)

double NumTable1::AppearFrequency(int n){
  if(init_datas.find(n) != init_datas.end()){
    return (double)init_datas[n] / init_count;
  } else {
    return 0.0;
  }
} // NumTable1::AppearFrequency (int)

double NumTable1::AppearFrequency(vector<int> vec){
  if (vec.size() < 1) {
    // invalid argument
    return -1.0;
  }
  double freq {1.0};
  freq *= AppearFrequency(vec.at(0));
  for(unsigned int i = 1; i < vec.size(); i++){
    if(freq == 0.0) { break; }
    freq *= AppearFrequencyNoInit(array<int, 2>{vec.at(i-1), vec.at(i)});
  }
  return freq;
} // NumTable1::AppearFrequency (vector<int>)

int NumTable1::CountingNoInit(array<int, 2> arr){
  if(datas.find(arr) != datas.end()){
    return datas[arr];
  } else {
    return 0;
  }
} // NumTable1::CountingNoInit(array<int, 2> arr)

int NumTable1::Counting(int n){
  if(init_datas.find(n) != init_datas.end()){
    return init_datas[n];
  } else {
    return 0;
  }
} // NumTable1::Counting(int n)

int NumTable1::Counting(vector<int> vec){
  if (vec.size() < 1) {
    // invalid argument
    return -1;
  }
  int c {1};
  c *= Counting(vec.at(0));
  for(unsigned int i = 1; i < vec.size(); i++){
    if(c == 0) { break; }
    c *= CountingNoInit(array<int, 2>{vec.at(i-1), vec.at(i)});
  }
  return c;
} // NumTable1::Counting(vector<int> vec)

int NumTable1::Counting(array<int, 6> arr){
  int c {1};
  c *= Counting(arr[0]);
  for(int i = 0; i < 5; i++){
    c *= CountingNoInit(array<int, 2>{arr[i], arr[i+1]});
  }
  return c;
} // NumTable1::Counting(array<int, 6> arr)

int NumTable1::CountingAllSequence(vector<int> vec){
  // Sum of every count of possible sequences
  int c {0};
  Shake6 s6 {cal6_impl_misc::VectorToArray6(vec)};
  for(auto d : s6.datas){
    c += Counting(d);
  }
  return c;
} // NumTable1::CountingAllSequence(vector<int> vec)

int NumTable1::CountingAllSequence(array<int, 6> arr){
  int c {0};
  Shake6 s6 {arr};
  for(auto d : s6.datas){
    c += Counting(d);
  }
  return c;
} // NumTable1::CountingAllSequence(array<int, 6> arr)

FrequencyCompare::FrequencyCompare (bool revflag) : reverse {revflag} { };

bool FrequencyCompare::operator() (const irow& a, const irow& b) {
  if (reverse) {
    return a.second < b.second;
  } else {
    return a.second > b.second;
  }
} // FrequencyCompare::operator()

RankingTable::RankingTable (unsigned int capacity_mul2, NumTable1 nt, 
                        std::array<int, 6> begin, std::array<int, 6> end, bool revflag=false)
: fc {}, datas {}, capacity {capacity_mul2 / 2} {
  AddDatas(nt, begin, end);
}; // RankingTable::RankingTable(unsigned int capacity_mul2, NumTable1 nt, bool revflag, array<int, 6> begin, array<int, 6> end);

RankingTable::RankingTable(unsigned int capacity_mul2, NumTable1 nt, bool revflag=false)
: RankingTable(capacity_mul2, nt, array<int, 6>{1,2,3,4,5,6}, array<int, 6>{0,0,0,0,0,0}, revflag) {
}; // RankingTable::RankingTable(int cp1, NumTable1 nt, bool revflag)

RankingTable::RankingTable(NumTable1 nt, std::array<int, 6> begin, std::array<int, 6> end)
: RankingTable(10000, nt, begin, end){
}; // RankingTable::RankingTable(NumTable1 nt, std::array<int, 6> begin, std::array<int, 6> end);

RankingTable::RankingTable(NumTable1 nt)
: RankingTable(10000, nt){
}; // RankingTable::RankingTable(NumTable1 nt)

void RankingTable::AddDatas(NumTable1 nt, array<int, 6> begin, array<int, 6> end){
  if(capacity < 1) {return;}
  unsigned int size {(unsigned int)datas.size()};
  array<int, 6> pivot (begin);
  int debug_1 {pivot[1]};
  while(pivot != end && pivot[0] != 0){
    if(debug_1 != pivot[1]){
      debug_1 = pivot[1];
      cout << "now : ";
      for(auto d : pivot){
        cout << d << ' ';
      }
      cout << endl;
    }
    int count = nt.CountingAllSequence(pivot);
    datas.push_back(irow {pivot, count});
    ++size;
    if(size >= capacity*2){
      SortTable();
      CutDatas();
      size = capacity;
    }
    cal6_impl_misc::Next6_45(pivot);
  }
  SortTable();
  CutDatas();
} // RankingTable::AddDatas

void RankingTable::SortTable (void){
  std::sort(datas.begin(), datas.end(), fc);
} // RankingTable::SortTable

void RankingTable::SetCapacity (unsigned int cap){
  if(cap < capacity){
    CutDatas();
  }
  capacity = cap;
} // RankingTable::SetCapacity

void RankingTable::CutDatas(void){
  if(datas.size() >= capacity){
    //std::move (datas.begin(), datas.begin() + capacity, datas.begin());
    datas.resize(capacity);
  }
} // RankingTable::CutDatas

void RankingTable::PrintRanking(unsigned int n){
  unsigned int mv = std::min ((long unsigned int)n, datas.size());
  for(auto d = datas.begin(); d < datas.begin()+mv; d++){
    for(auto i : d->first){
      cout << i << ' ';
    }
    cout << " : " << d->second << endl;
  }
} // RankingTable::PrintRanking(int n)

void RankingTable::PrintRanking(void){
  PrintRanking(capacity);
} // RankingTable::PrintRanking(void)

void RankingTable::SaveRanking(string s, unsigned int n){
  ofstream ofs {s, std::ofstream::out | std::ofstream::app};
  unsigned int minv = std::min ((long unsigned int)n, datas.size());
  unsigned int c {1};
  for(auto d = datas.begin(); d < datas.begin()+minv; d++){
    ofs << 'L' << c << '\t';
    for(auto i : d->first){
      ofs << i << ' ';
    }
    ofs << " : " << d->second << endl;
    ++c;
  }
} // RankingTable::SaveRanking(string s, unsigned int n)

void RankingTable::SaveRanking(string s){
  SaveRanking(s, datas.size());
} // RankingTable::SaveRanking(string s)

void RankingTable::PrintForDebug(void){
  cout << datas.size();
  cout << endl;
} // RankingTable::PrintForDebug

void Shake6::ShakeRecursively(const array<int, 6>& arr, array<int, 6>& tmparr, int loc){
  if (loc > 5) {
    // base-case
    datas.push_back(tmparr);
    ++count;
    return;
  } else {
    for(int i = 0; i < 6; i++){
      if(tmparr[i] == 0){
        tmparr[i] = arr[loc];
        ShakeRecursively(arr, tmparr, loc+1);
        tmparr[i] = 0;
      }
    }  
  }
} // Shake6::Shake_recursively

Shake6::Shake6 (const std::array<int, 6>& arr)
:  count {0}, datas {} {
  array<int, 6> tmparr {0,0,0,0,0,0};
  ShakeRecursively(arr, tmparr, 0);
}; // Shake6::Shake6

void Shake6::PrintDatas(void){
  for(auto d : datas){
    for(int i = 0 ; i < 6; ++i){
      cout << d[i] << ' ';
    }
    cout << endl;
  }
} // Shake6::PrintDatas

void cal6_impl_misc::Next6_45 (array<int, 6>& arr){
  bool touch {false};
  for(int i = 5; i > -1; i--){
    if(arr[i] < 40 + i){
      int t = ++arr[i];
      for(int j = i+1; j < 6; j++){
        arr[j]= ++t;
      }
      touch = true;
      break;
    }
  }
  if (!touch){
    for(int i = 0; i < 5; i++){
      arr[i] = 0;
    }
  }
} // Next6_45

vector<int> cal6_impl_misc::Array6ToVector(const array<int, 6>& arr){
  vector<int> vec {};
  for (auto a : arr){
    vec.push_back(a);
  }
  return vec;
} // Array6ToVector

array<int, 6> cal6_impl_misc::VectorToArray6(const vector<int>& vec){
  array<int, 6> arr {};
  int c {0};
  for(auto v : vec){
    arr[c] = v; 
    ++c;
    if (c > 5) {break;}
  }
  return arr;
} // VectorToArray6

int main (int argc, char* argv[]) {
  Raw r {"raw.txt"};
  NumTable1 nt {r};
  RankingTable rt {nt, array<int, 6>{1,2,3,4,5,6}, array<int, 6>{2,3,4,5,6,7}};
  rt.SaveRanking("ranking_middle.txt", 1000);
  for(int i = 2; i < 41; ++i){
    cout << "cal6 - " << i << " is now processing" << endl;
    rt.AddDatas(nt, array<int, 6>{i,i+1,i+2,i+3,i+4,i+5}, array<int, 6>{i+1,i+2,i+3,i+4,i+5,i+6});
    rt.SaveRanking("ranking_middle.txt", 1000);
  }
  rt.SaveRanking("ranking.txt", 10000);


#ifdef DEBUGING
  Raw r {"raw.txt"};
  //r.PrintDatas();
  NumTable1 nt {r};
  //nt.PrintDatas();
  cout << "Counting of 3-2-5 is " << nt.Counting(vector<int>{3, 2, 5}) << endl;
  //----Shake6 test
  //Shake6 s6 {array<int, 6> {1, 2, 3, 4, 5, 6}};
  //s6.PrintDatas();
  //----RankingTable test
  RankingTable rt {nt, array<int, 6>{1,2,3,4,5,6}, array<int, 6>{1,3,4,5,6,7}};
  //rt.PrintForDebug();
  rt.PrintRanking(20);
  rt.SaveRanking("ranking.txt", 1000);
#endif
  return 0;
} // main
#ifndef ___CUSHIONBADAK_CAL6_H
#define ___CUSHIONBADAK_CAL6_H

#include <array>
#include <map>
#include <queue>
#include <string>
#include <vector>

namespace cal6 {
  enum class EC {NEED6, NANUM}; //ErrorCase
  class Error {
    private:
      EC error_case;
    public:
      Error (EC ec);
      void FileReadErrorMessage (void);
  }; // class Error
  
  class Raw {
    private:
      int size;
      std::vector< std::array<int, 6> > datas;
      std::array<int, 6> MakeSixIntegers(const std::string &s);
    public:
      Raw (const char* filename);
      void PrintDatas (void);
      const std::vector< std::array<int, 6> >& GetDatas(void);
  }; // class Raw
  
  class NumTable1 {
    // 1-context datas.
    private:
      std::map<int, int> init_datas;
      std::map<std::array<int, 2>, int> datas;
      int init_count;
      int count;
    public:
      NumTable1 (Raw r);
      void PrintDatas (void);
      double AppearFrequencyNoInit(std::array<int, 2> arr);
      double AppearFrequency(int n);
      double AppearFrequency(std::vector<int> vec);
      // Counting-s are int version of AppearFrequency-s.
      int CountingNoInit(std::array<int, 2> arr);
      int Counting(int n);
      int Counting(std::vector<int> vec);
      int Counting(std::array<int, 6> arr);
      int CountingAllSequence(std::vector<int> vec);
      int CountingAllSequence(std::array<int, 6> arr);
  }; // class NumTable1
  
  class FrequencyCompare {
    typedef std::pair<std::array<int, 6>, int> irow;
    private:
      bool reverse;
    public:
      FrequencyCompare (bool revflag=false);
      bool operator() (const irow& a, const irow& b);
  }; // class FrequencyCompare
  
  class RankingTable {
    // BAD IMPLEMENT. This class supports numbers between 1-45 only.
    typedef std::pair<std::array<int, 6>, int> irow;
    private:
      FrequencyCompare fc;
      std::vector<irow> datas;
      unsigned int capacity;
    public:
      RankingTable(unsigned int capacity_mul2, NumTable1 nt, 
                  std::array<int, 6> begin, std::array<int, 6> end, bool revflag);
      RankingTable(unsigned int capacity_mul2, NumTable1 nt, bool revflag);
      RankingTable(NumTable1 nt, std::array<int, 6> begin, std::array<int, 6> end);
      RankingTable(NumTable1 nt);
      void AddDatas(NumTable1 nt, std::array<int, 6> begin, std::array<int, 6> end);
      void SortTable(void);
      void SetCapacity(unsigned int cap);
      void CutDatas(void);
      void PrintRanking(unsigned int n);
      void PrintRanking(void);
      void SaveRanking(std::string s, unsigned int n);
      void SaveRanking(std::string s);
      void PrintForDebug(void);
  }; // class RankingTable
  
  class Shake6 {
    private:
      int count;
      void ShakeRecursively(const std::array<int, 6>& arr, std::array<int, 6>& tmparr, int loc);
    public:
      std::vector<std::array<int, 6>> datas;
      Shake6(const std::array<int, 6>& arr);
      void PrintDatas(void);
  }; // class Shake6
  
  namespace cal6_impl_misc{
    void Next6_45 (std::array<int, 6>& arr);
    std::vector<int> Array6ToVector (const std::array<int, 6>& arr);
    std::array<int, 6> VectorToArray6 (const std::vector<int>& vec);
  }; // namespace cal6_misc
}; // namespace cal6

#endif
